package com.example.demo_s67.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_s67.model.CAlbum;
import com.example.demo_s67.model.CPicture;
import com.example.demo_s67.repository.IAlbumRepository;
import com.example.demo_s67.repository.IPictureRepository;

@CrossOrigin
@RestController
public class CPictureController {
    @Autowired
    IAlbumRepository pAlbumRepository;

    @Autowired
    IPictureRepository pAnhRepository;

    @GetMapping("/pictures")
    public ResponseEntity<Set<CPicture>> getAnhsByAlbumId(
            @RequestParam(value = "id") Long albumId) {
        try {
            CAlbum cAlbum = pAlbumRepository.getReferenceById(albumId);
            // CCustomer vCCustomer = pCustomerRepository.findByCustomerId(customerId);

            if (cAlbum != null) {
                return new ResponseEntity<>(cAlbum.getAnhs(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/pictures/details/{id}")
    public CPicture getPictureById(@PathVariable Long id) {
        if (pAnhRepository.findById(id).isPresent())
            return pAnhRepository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/pictures/all")
    public List<CPicture> getAllPictures() {
        return pAnhRepository.findAll();
    }

    @PostMapping("/pictures/create/{id}")
    public ResponseEntity<Object> createPicture(@PathVariable("id") Long id, @RequestBody CPicture cAnh) {
        try {
            Optional<CAlbum> albumData = pAlbumRepository.findById(id);
            if (albumData.isPresent()) {

                CPicture newRole = new CPicture();
                newRole.setAnhCode(cAnh.getAnhCode());
                newRole.setAnhName(cAnh.getAnhName());
                newRole.setLinkAnh(cAnh.getLinkAnh());
                newRole.setMoTa(cAnh.getMoTa());
                newRole.setNgayTao(new Date());
                newRole.setNgayCapNhat(null);
                CAlbum _picture = albumData.get();
                newRole.setAlbum(_picture);

                CPicture savedRole = pAnhRepository.save(newRole);

                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Pictures: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/pictures/update/{id}")
    public ResponseEntity<Object> updatePicture(@PathVariable("id") Long id, @RequestBody CPicture cAnh) {
        Optional<CPicture> pictureData = pAnhRepository.findById(id);
        if (pictureData.isPresent()) {

            CPicture newPicture  = pictureData.get();
            newPicture.setAnhCode(cAnh.getAnhCode());
            newPicture.setAnhName(cAnh.getAnhName());
            newPicture.setLinkAnh(cAnh.getLinkAnh());
            newPicture.setMoTa(cAnh.getMoTa());
            newPicture.setNgayCapNhat(new Date());
            CPicture savedAnh = pAnhRepository.save(newPicture);

            return new ResponseEntity<>(savedAnh, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/pictures/delete/{id}")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable Long id) {
        try {
            pAnhRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
