package com.example.demo_s67.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_s67.model.CAlbum;
import com.example.demo_s67.repository.IAlbumRepository;
import com.example.demo_s67.repository.IPictureRepository;

@CrossOrigin
@RestController
public class CAlbumController {
    @Autowired
    IAlbumRepository pAlbumRepository;

    @Autowired
    IPictureRepository pAnhRepository;

    @GetMapping("/albums")
    public ResponseEntity<List<CAlbum>> getAllCAlbums() {
        try {
            List<CAlbum> pCAlbum = new ArrayList<CAlbum>();

            pAlbumRepository.findAll().forEach(pCAlbum::add);

            return new ResponseEntity<>(pCAlbum, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/albums/details/{id}")
    public CAlbum getAlbumById(@PathVariable Long id) {
        if (pAlbumRepository.findById(id).isPresent())
            return pAlbumRepository.findById(id).get();
        else
            return null;
    }

    @PostMapping(value = "/albums/create")
    public ResponseEntity<Object> createAlbum(@RequestBody CAlbum cAlbum) {
        try {

            CAlbum newRole = new CAlbum();
            newRole.setAlbumCode(cAlbum.getAlbumCode());
            newRole.setAlbumName(cAlbum.getAlbumName());
            newRole.setMoTa(cAlbum.getMoTa());
            newRole.setNgayTao(new Date());
            newRole.setNgayCapNhat(null);
            CAlbum savedRole = pAlbumRepository.save(newRole);

            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Album: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/albums/update/{id}")
    public ResponseEntity<Object> updateAlbum(@PathVariable("id") Long id, @RequestBody CAlbum cAlbum) {
        Optional<CAlbum> albumData = pAlbumRepository.findById(id);
        if (albumData.isPresent()) {

            CAlbum newProvince = albumData.get();
            newProvince.setAlbumCode(cAlbum.getAlbumCode());
            newProvince.setAlbumName(cAlbum.getAlbumName());
            newProvince.setMoTa(cAlbum.getMoTa());
            newProvince.setNgayCapNhat(new Date());
            CAlbum savedProvince = pAlbumRepository.save(newProvince);

            return new ResponseEntity<>(savedProvince, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/albums/delete/{id}")
    public ResponseEntity<Object> deleteAlbumById(@PathVariable Long id) {
        try {
            Optional<CAlbum> optional = pAlbumRepository.findById(id);
            if (optional.isPresent()) {
                pAlbumRepository.deleteById(id);
            } else {
                // countryRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
