package com.example.demo_s67.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "pictures")
public class CPicture {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "anh_Code")
    private String anhCode;
    @Column(name = "anh_Name")
    private String anhName;
    @Column(name = "link_anh")
    private String linkAnh;
    @Column(name = "mo_Ta")
    private String moTa;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayTao;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_cap_nhat", nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayCapNhat;
    @ManyToOne
    @JoinColumn(name = "album_id")
    @JsonBackReference
    private CAlbum album;

    public CPicture() {
    }

    public CPicture(String anhCode, String anhName, String linkAnh, String moTa, Date ngayTao, Date ngayCapNhat) {
        this.anhCode = anhCode;
        this.anhName = anhName;
        this.linkAnh = linkAnh;
        this.moTa = moTa;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAnhCode() {
        return anhCode;
    }

    public void setAnhCode(String anhCode) {
        this.anhCode = anhCode;
    }

    public String getAnhName() {
        return anhName;
    }

    public void setAnhName(String anhName) {
        this.anhName = anhName;
    }

    public String getLinkAnh() {
        return linkAnh;
    }

    public void setLinkAnh(String linkAnh) {
        this.linkAnh = linkAnh;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public CAlbum getAlbum() {
        return album;
    }

    public void setAlbum(CAlbum album) {
        this.album = album;
    }

}
