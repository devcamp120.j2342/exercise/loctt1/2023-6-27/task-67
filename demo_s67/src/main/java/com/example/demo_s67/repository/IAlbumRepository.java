package com.example.demo_s67.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo_s67.model.CAlbum;

public interface IAlbumRepository extends JpaRepository<CAlbum, Long> {

}
